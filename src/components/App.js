import React, { Component } from 'react'
import '../styles/App.css'
import styled from 'styled-components'
import DogList from './DogList'
import CreateDog from './CreateDog'
import SignIn from './SignIn'
import Header from './Header'
import DogGif from '../dog.gif' 
import { Switch, Route } from 'react-router-dom'

const StyledApp = styled.div({
  background: '#00bd70',
  display: 'grid',
  position: 'absolute',
  top: 0,
  bottom: 0,
  left: 0,
  right: 0,
  overflow: 'hidden',
  gridTemplateRows: '100px auto',
  gridTemplateAreas: "'header' 'main'"
})

const Main = styled.div({
  displayArea: 'main',
  height: '100%',
  width: '100%',
  display: 'grid',
  gridTemplateColumns: '40% 60%',
})

const Sidebar = styled.div({
  paddingLeft: '100px',
  height: '100%',
  paddingRight: '100px',
  backgroundImage: `url(${DogGif})`,
  backgroundRepeat: 'no-repeat',
  backgroundPosition: 'center bottom',
  backgroundSize: '100%',
})

const Feed = styled.div({
  height: '100%',
  position: 'relative'
})

class App extends Component {
  render() {
    return (
      <StyledApp>
        <Header />
        <Main>
          <Sidebar>
            <Switch>
              <Route exact path='/create' component={CreateDog} />
              <Route exact path='/sign-in' component={SignIn} />
            </Switch>
          </Sidebar>
          <Feed>
            <DogList />
          </Feed>
        </Main>
      </StyledApp>
    )
  }
}

export default App
