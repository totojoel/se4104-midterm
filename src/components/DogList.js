import React, { Component } from 'react'
import styled from 'styled-components'
import { Query } from 'react-apollo'
import gql from 'graphql-tag'
import Dog from './Dog'

const DOG_QUERY = gql`
  query {
    dogs {
      id
      name
      breed
      gender
      pic
      rescuer {
        name
      }
      adopter {
        name
      }
    } 
  }
`

const List = styled.div({
  height: '100%',
  position: 'absolute',
  width: '100%',
  overflowY: 'scroll'
})

class DogList extends Component {
  // _updateCacheAfterEdit = (store, updateDog, dogId) => {
  //   const data = store.readQuery({ query: DOG_QUERY })

  //   const editedDog = data.feed.dogs.find(dog => dog.id === dogId)
  // }

  _subscribeToNewDogs = async () => {

  }

  render() {
    return (
      <Query query={DOG_QUERY}>
        { ({ loading, error, data, subscribeToMore }) => {
          if (loading) return <div>Fetching</div>
          if (error) return <div>Error</div>

          this._subscribeToNewDogs(subscribeToMore)
    
          const dogsToRender = data.dogs
          console.log(data)
    
          return (
            <List>
              {dogsToRender.map(dog => <Dog key={dog.id} dog={dog}/>)}
            </List>
          )
        }}
      </Query>
    )
  }
} 

export default DogList
