import React, { Component } from 'react'
import  { AUTH_TOKEN } from '../constants'
import { Form, Title, Row, Input, Button } from './formStyles'  
import { Mutation } from 'react-apollo'
import gql from 'graphql-tag'

const SIGNUP_MUTATION = gql`
  mutation SignupMutation($email: String!, $name: String!, $password: String!) {
    signup(email: $email, name: $name, password: $password) {
      token
    }
  }
`

const SIGNIN_MUTATION = gql`
  mutation SigninMutation($email: String!, $password: String!) {
    signin(email: $email, password: $password) {
      token
    }
  }
`

class SignIn extends Component {
  state = {
    signin: true,
    email: '',
    password: '',
    name: ''
  }

  render() {
    const { signin, email, password, name } = this.state

    return (
      <Form>
        <Title>{signin ? 'Sign in to your account.' : 'Create your account.'}</Title>
        {!signin && (
          <Row>
            <Input
              value={name}
              onChange={e => this.setState({ name: e.target.value })}
              type='text'
              placeholder='Name'
            />
          </Row>
        )}
        <Row>
          <Input
            value={email}
            onChange={e => this.setState({ email: e.target.value })}
            type='email'
            placeholder='Email'
          />
        </Row>
        <Row>
          <Input
            value={password}
            onChange={e => this.setState({ password: e.target.value })}
            type='password'
            placeholder='Password'
          />
        </Row>
        <Row>
          <Button onClick={() => this.setState({ signin: !signin })}>
            {signin ? `No account?` : 'Have account?'}
          </Button>
          <Mutation
            mutation={signin ? SIGNIN_MUTATION : SIGNUP_MUTATION}
            variables={{ email, password, name }} 
            onCompleted={data => this._confirm(data)}
          >
            {mutation => (
              <Button onClick={mutation}>
                {signin ? 'Sign in' : 'Create Account'}
              </Button>
              )
            }
          </Mutation>
        </Row>
      </Form>
    )
  }

  _confirm = data => {
    const { token } = this.state.signin ? data.signin : data.signup
    this.props.history.push('/')
    window.location.reload()
    this._saveUserData(token)
  }

  _saveUserData = token => {
    localStorage.setItem(AUTH_TOKEN, token)
  }
}

export default SignIn

