import React from 'react'
import styled from 'styled-components'

const Preview = styled.div({
  border: 'solid 1px #009e52',
  width: '350px',
  float: 'left',
  marginBottom: '20px',
  marginRight: '20px',
  background: '#ffffff',
  display: 'grid',
  gridTemplateColumns: '150px auto',
})

const Frame = styled.div({
  background: 'black',
  height: '150px',
  overflow: 'hidden',
})

const Pic = styled.img({
  width: '100%',
  background: 'red'
})

const Info = styled.div({
  padding: '20px',
  fontSize: '10pt'
})

const Name = styled.div({
  fontSize: '11pt',
})

const Dog = props => (
  <Preview>
    <Frame>
      <Pic src={props.dog.pic} />
    </Frame>
    <Info>
      <Name>{ props.dog.name }</Name>
      <div>{ props.dog.gender } { props.dog.breed }</div>
      <div>Rescued by { props.dog.rescuer.name } </div>
    </Info>
  </Preview>
)

export default Dog
