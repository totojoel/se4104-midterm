import React from 'react'
import { Link } from 'react-router-dom'
import { withRouter } from 'react-router'
import styled from 'styled-components'
import { AUTH_TOKEN } from '../constants';

const StyledHeader = styled.div({
  display: 'grid',
  gridTemplateColumns: '40% auto',
  displayArea: 'header',
  alignContent: 'center'
})

const Title = styled(Link)({
  textDecoration: 'none',
  fontSize: '28px',
  letterSpacing: '-1px',
  marginLeft: '100px', 
  textTransform: 'lowercase',
  fontWeight: '700',
  color: 'white',
  display: 'grid',
  alignContent: 'center'
})

const StyledLink = styled(Link)({
  textDecoration: 'none',
  fontSize: '16px',
  fontWeight: '700',
  display: 'grid',
  alignContent: 'center',
  color: 'white',
  marginRight: '18px'
})

const StyledNavigation = styled.div({
  display: 'flex',
})

const authToken = localStorage.getItem(AUTH_TOKEN)

const Navigation = () => (
  <StyledNavigation>
    { authToken && <StyledLink to='/create'>Add New Dog</StyledLink>}
    { authToken ? <StyledLink to='sign-in' onClick={() => {
        localStorage.removeItem(AUTH_TOKEN)
        this.props.history.push('/')
      }}>Sign out</StyledLink> :
      <StyledLink to='/sign-in'>Sign in</StyledLink>
    }
  </StyledNavigation>
)

const Header = () => (
  <StyledHeader>
    <Title to='/'>Free Friends</Title>
    <Navigation />
  </StyledHeader>
)

export default withRouter(Header)
