import React, { Component } from 'react'
import { Mutation } from 'react-apollo'
import gql from 'graphql-tag'
import { Form, Title, Row, Input, Button } from './formStyles'  

const POST_MUTATION = gql`
  mutation PostMutation($name: String!, $breed: String, $gender: String!, $pic: String) {
    createDog(name: $name, breed: $breed, gender: $gender, pic: $pic) {
      id
      createdAt
    }
  }
`  

class CreateDog extends Component {
  state = {
    name: '',
    breed: '',
    gender: '',
    pic: ''
  }
  
  render() {
    const { name, breed, gender, pic } = this.state
    return (
      <Form>
        <Title>Add new dog.</Title>
        <Row>
          <Input
            value={name}
            onChange={e => this.setState({ name: e.target.value })}
            type='text'
            placeholder='Name'
          />
        </Row>
        <Row>
          <Input
            value={breed}
            onChange={e => this.setState({ breed: e.target.value })}
            type='text'
            placeholder='Breed'
          />
        </Row>
        <Row>
          <Input
            value={gender}
            onChange={e => this.setState({ gender: e.target.value })}
            type='text'
            placeholder='Gender'
          />
        </Row>
        <Mutation mutation={POST_MUTATION} variables={{ name, breed, gender, pic }}>
          {postMutation => <Row><Button onClick={postMutation}>Submit</Button></Row>}
        </Mutation>
      </Form>
    )
  }
}

export default CreateDog
