import styled from 'styled-components'

export const Form = styled.div({
  border: 'solid 1px #009e52',
  background: 'white',
  padding: '20px',
  width: '100%',
  display: 'grid'
})

export const Title = styled.div({
  marginBottom: '20px',
  fontSize: '14px',
  fontWeight: '700',
}) 

export const Row = styled.div({
  textAlign: 'right'
})

export const Input = styled.input({
  border: 'none',
  borderBottom: 'solid 1pt #009e52',
  color: 'black',
  paddingBottom: '8px',
  width: '100%',
  background: 'none',
  fontSize: '12px',
  marginBottom: '10px'
})

export const Button = styled.button({
  border: 'none',
  padding: '10px',
  fontSize: '12px',
  background: '#fec92f',
  fontWeight: '700',
  marginLeft: '10px',
  marginTop: '10px'
})
