const bcrypt = require('bcryptjs')
const jwt = require('jsonwebtoken')
const { APP_SECRET, getUserId } = require('../utils')

const adoptDog = (parent, args, context, info) => {
  const userId = getUserId(context)

  return context.db.mutation.updateDog({
    data: {
      adopter: { connect: { id: userId } }
    },
    where: {
      id: args.id
    }
  }, info)
}

const createDog = (parent, args, context, info) => {
  const userId = getUserId(context)
  
  return context.db.mutation.createDog({
    data: {
      name: args.name,
      breed: args.breed,
      gender: args.gender,
      pic: args.pic,
      rescuer: { connect: { id: userId } }
    }
  }, info)
}

const updateDog = (parent, args, context, info) => (
  context.db.mutation.updateDog({
    data: {
      name: args.name,
      breed: args.breed,
      gender: args.gender,
      pic: args.pic
    },
    where: {
      id: args.id
    }
  }, info)
)

const deleteDog = (parent, args, context, info) => (
  context.db.mutation.deleteDog({
    where: {
      id: args.id
    }
  }, info)
)

const signup = async (parent, args, context, info) => {
  const password = await bcrypt.hash(args.password, 10)
  const user = await context.db.mutation.createUser({
    data: { ...args, password }
  }, `{ id }`)
  const token= jwt.sign({ userId: user.id }, APP_SECRET)

  return {
    token,
    user
  }
}

const signin = async (parent, args, context, info) => {
  const user = await context.db.query.user({ 
    where: { email: args.email } 
  }, ` { id password } `)
  if (!user) {
    throw new Error('No such user found')
  }

  const valid = await bcrypt.compare(args.password, user.password)
  if (!valid) {
    throw new Error('Invalid password')
  }

  const token = jwt.sign({ userId: user.id }, APP_SECRET)

  return {
    token,
    user
  }
}

module.exports = {
  adoptDog,
  createDog,
  updateDog,
  deleteDog,
  signup,
  signin
}