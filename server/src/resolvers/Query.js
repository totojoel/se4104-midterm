const dogs = (parent, args, context, info) => (
  context.db.query.dogs({}, info)
)

const users = (parent, args, context, info) => (
  context.db.query.users({}, info)
)

module.exports = {
  dogs,
  users
}
